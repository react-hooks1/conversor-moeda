import React from 'react';

function ListarMoedas(){

    const MOEDAS =[
        {"sigla": "AUD", "descricao":"Dólar australiano"},
        {"sigla": "BRL", "descricao":"Real Brasileiro"}
    ];

    function compare(m1, m2){

        if(m1.descricao < m2.descricao){
            return -1;
        }else if(m1.descricao > m2.descricao){
            return 1
        }

        return 0;
    }

    return MOEDAS.sort(compare).map(moeda =>
        <option value={moeda.sigla} key={moeda.sigla}>
            {moeda.descricao}
        </option>
        );
}
export default ListarMoedas;